context PSM!BoundOperation {
	
	constraint AbstractOperationIsValid {
		guard: self.satisfies("NamedElementHasContainer")
		check: self.abstract implies self.eContainer.getInheritedOperationImplementationsByName(self.name).size() == 0
		message: "Bound operation cannot be abstract if it's overriding a non-abstract bound operation"
	}
    
    constraint AbstractOperationHasNoImplementation {
    	check: self.abstract implies self.implementation.isUndefined()
    	message: "Abstract bound operation: " + self.name + " cannot have implementation."
    }
    
    constraint InstanceRepresentationMatchesOwnerEntityType {
    	guard: self.satisfies("NamedElementHasContainer")
    	check: self.instanceRepresentation.entityType == self.eContainer
    	message: "Instance representation of bound operation: " + self.name + " must match the owner entity type: " + self.eContainer.name
    }
 
 	constraint OverridingWithValidParameters {
     	guard: self.satisfiesAll("NamedElementHasContainer","AbstractOperationIsValid")
     	     			and psmUtils.getInheritedBoundOperationsByName(self.eContainer, self.name).forAll(o | o.satisfies("OverridingWithValidParameters"))
     	check: psmUtils.getInheritedBoundOperationsByName(self.eContainer, self.name).forAll(o | psmUtils.parametersAreCompatible(self,o))
        message: "Overriding of bound operation cannot change parameters (bound operation " + self.name + " in " + self.eContainer.name + ")"
     }
}
