context PSM!TransferOperationBehaviour {

	guard: self.eContainer.isDefined() and 
		(self.behaviourType == PSM!TransferOperationBehaviourType#SET_RELATION_OF_RELATION or
		self.behaviourType == PSM!TransferOperationBehaviourType#UNSET_RELATION_OF_RELATION or
		self.behaviourType == PSM!TransferOperationBehaviourType#ADD_ALL_TO_RELATION_OF_RELATION or
		self.behaviourType == PSM!TransferOperationBehaviourType#REMOVE_ALL_FROM_RELATION_OF_RELATION)
		and self.satisfiesAll("OperationIsValidBoundBehaviour","OwnerIsRelationBoundBehaviour","OwnerIsValidBoundBehaviour")
	
	constraint RelationIsDefinedBoundWithRelation {
		check: self.relation.isDefined() 
		message: "Relation must be defined for '" + self.behaviourType + "' operation: " + self.eContainer.name  + " (in: " + self.eContainer.eContainer.name + ")."
	}
	
	constraint RelationIsValidBoundWithRelation {
		guard: self.satisfies("RelationIsDefinedBoundWithRelation")
		check: psmUtils.getAllTransferObjectRelations(self.owner.target).includes(self.relation)
		message: "Relation of '" + self.behaviourType + "' operation: " + self.eContainer.name  + " must be one of the transfer object type referenced by the operation's owner."
	}
	
	constraint InputParameterIsDefinedBoundWithRelation {
		guard: self.eContainer.satisfies("BindingIsValid")
    	check: self.eContainer.binding.input.isDefined()
    	message: "'" + self.behaviourType + "' operation's binding must have an input parameter named 'input' (operation: " + self.eContainer.name + ")."
    }
    
    constraint InputNameIsValidBoundWithRelation {
    	guard: self.satisfies("InputParameterIsDefinedBoundWithRelation") and self.eContainer.satisfies("BindingIsValid")
    	check: self.eContainer.binding.input.name == "input"
    	message: "Input of '" + self.behaviourType + "' operation's binding must be named 'input' (operation: " + self.eContainer.name + ")."
    }
    
    constraint InputTypeIsValidBoundWithRelation {
    	guard: self.satisfies("InputParameterIsDefinedBoundWithRelation") and self.eContainer.satisfies("BindingIsValid")
    	check: self.eContainer.binding.input.type.getAllSuperTransferObjectTypes().including(self.eContainer.binding.input.type).includes(self.owner.target)
    	message: "Input type of '" + self.behaviourType + "' operation's binding must be kind of mapped transfer object type referenced by the owner (operation: " + self.eContainer.name + ")."
    }
    
    constraint InputCardinalityIsValidBoundWithRelation {
    	guard: self.satisfies("InputParameterIsDefinedBoundWithRelation") and self.eContainer.satisfies("BindingIsValid")
    	check: self.eContainer.binding.input.cardinality.lower == 1 and
    		self.eContainer.binding.input.cardinality.upper == 1 
    	message: "Cardinality of the input of '" + self.behaviourType + "' operation's binding must be 1..1 (operation: " + self.eContainer.name + ")."
    }
    
    constraint OutputParameterIsNotDefinedBoundWithRelation {
    	guard: self.eContainer.satisfies("BindingIsValid")
    	check: self.eContainer.binding.output.isUndefined() 
    	message: "'" + self.behaviourType + "' operation's binding cannot have an output parameter (operation: " + self.eContainer.name + ")."
    }
}
	